const express = require('express')
const morgan = require('morgan')

const app = express()

app.set('port',process.env.PORT || 3000);

app.use(morgan('dev'))

// Con esto permito a los clientes conectarse con esta api
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', '*')
    if(req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
        return res.status(200).json({})
    }
    next();
});

app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.use('/materias', require("./routes/materias"))

module.exports = app;