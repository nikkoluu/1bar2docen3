const { Router } = require('express')
const router = Router()

const controlador_materias =require('../controllers/materiasController.js')

router.get('/', controlador_materias.get_materias)

router.get('/:id',controlador_materias.get_materia)

router.post('/', controlador_materias.agregar_materia )

router.delete('/:id',controlador_materias.borrar_materia)

router.put('/:id',controlador_materias.editar_materia)

module.exports =  router