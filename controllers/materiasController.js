const Materias_controller = {}

const materia = require('../models/materia')

Materias_controller.get_materias = async (req,res) => {
    const materias = await materia.find()
    res.json(materias)
}
Materias_controller.get_materia  = async (req,res) => {
    const materia_buscada = await materia.findById(req.params.id)
    res.send(materia_buscada)
}

Materias_controller.agregar_materia = async (req,res) => {

    const nuevaMateria = new materia(req.body)

    await nuevaMateria.save()
    
    res.send("materia creada")
}
Materias_controller.editar_materia= async (req,res) => {
    await materia.findByIdAndUpdate(req.params.id,req.body)
    res.json({status: 'materia editada'})
}
Materias_controller.borrar_materia= async(req,res) => {
    await materia.findByIdAndDelete(req.params.id)
    res.json({status: 'materia borrada'})
}

module.exports = Materias_controller; 